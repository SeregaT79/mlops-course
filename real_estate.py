import numpy as np
import pandas as pd
from pathlib import Path

FILE_NAME = 'all_v2.csv'
CUR_DIR = Path(__file__).resolve().parent
DATA_DIR = CUR_DIR / 'data/'
FILE_PATH = DATA_DIR / FILE_NAME
